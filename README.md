# Instructions

* npm install
* npm run build
* firebase emulators:start
* -> using the function URL works : http://localhost:5001/wesclowtools/us-central1/helloWorld 
* -> using the app URL throws CORS error : http://localhost:5000
# test_firebase_cloud_function_vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
